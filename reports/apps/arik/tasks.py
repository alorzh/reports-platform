from __future__ import absolute_import
from celery.task import task, periodic_task
from celery.utils.log import get_task_logger
from sqlalchemy import create_engine
from django.core.mail import send_mail
from .transformation import index as all_orders_transform
logger = get_task_logger(__name__)


@task()
def all_orders(connection_params, local_db, title, machine_name, user):
    engine = create_engine(
        connection_params['database_type'] + '://' + connection_params['username'] + ':' + connection_params[
            'password'] + '@' + connection_params['host'] + ':' + connection_params['port'] + '/' +
        connection_params['database_name'])
    local_db_engine = create_engine('mysql' + '://' + local_db['USER'] + ':' + local_db['PASSWORD'] +
                                    '@' + local_db['HOST'] + ':' + local_db['PORT'] + '/' +
                                    local_db['NAME'])

    result = all_orders_transform(engine, local_db_engine, machine_name)
    if result is True:
        # delete_duplicates.delay(local_db_engine, machine_name)
        if user is not None:
            send_mail('[REPORTS] %s platform tables done' % title, '%s platform tables transformation is done' % title,
                      user.email,
                      [user.email], fail_silently=False)
    elif result is False:
        if user is not None:
            send_mail('[REPORTS] %s platform tables has error' % title,
                      '%s platform tables transformation has error. Please check your tables' % title,
                      user.email,
                      [user.email], fail_silently=False)


"""
@task()
def delete_duplicates(local_db_engine, machine_name):
    return 'TBD'
"""
