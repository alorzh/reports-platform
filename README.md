run server:
$ sudo rabbitmqctl add_user myuser mypassword
$ sudo rabbitmqctl add_vhost myvhost
$ sudo rabbitmqctl set_user_tags myuser mytag
$ sudo rabbitmqctl set_permissions -p myvhost myuser ".*" ".*" ".*"

$ sudo rabbitmq-server
$ sudo rabbitmq-server -detached
$ sudo rabbitmqctl stop

//see more info here: http://docs.celeryproject.org/en/latest/getting-started/brokers/rabbitmq.html#id7

run Celery:
celery -A reports worker -l info -B (run above project)

http://docs.celeryproject.org/en/latest/getting-started/next-steps.html#next-steps
http://docs.celeryproject.org/en/latest/userguide/workers.html#guide-workers

Install and run Flower: http://docs.celeryproject.org/en/latest/userguide/monitoring.html#flower-real-time-celery-web-monitor

* celery -A reports flower --broker=amqp://root:root@localhost:5672/reports_host
* rabbitmq-plugins enable rabbitmq_management