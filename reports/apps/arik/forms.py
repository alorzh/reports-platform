from django import forms
from .models import Connections, ArikOrder
from django.db.models import Count


class ConnectionsForm(forms.ModelForm):
    class Meta:
        model = Connections
        fields = "__all__"
        widgets = {
            'password': forms.PasswordInput(render_value=True),
        }


class ArikTopNRoutesForm(forms.ModelForm):
    origin_query = ArikOrder.objects.order_by('origin').values_list('origin', flat=True).exclude(
        origin='').distinct()
    origin_choices = [('', 'None')] + [(id, id) for id in origin_query]

    destination_query = ArikOrder.objects.order_by('destination').values_list('destination', flat=True).exclude(
        destination='').distinct()
    destination_choices = [('', 'None')] + [(id, id) for id in destination_query]

    flight_type_query = ArikOrder.objects.order_by('flight_type').values_list('flight_type', flat=True).exclude(
        flight_type='').distinct()
    flight_type_choices = [('', 'None')] + [(id, id) for id in flight_type_query]

    order_status_query = ArikOrder.objects.order_by('order_status').values_list('order_status', flat=True).exclude(
        order_status='').distinct()
    order_status_choices = [('', 'None')] + [(id, id) for id in order_status_query]

    top_n = forms.IntegerField(initial=10, label='Top N', required=True)
    date_from = forms.DateField(widget=forms.DateInput(), required=False)
    date_to = forms.DateField(widget=forms.DateInput(), required=False)
    origin = forms.ChoiceField(origin_choices, required=False)
    destination = forms.ChoiceField(destination_choices, required=False)
    flight_type = forms.ChoiceField(flight_type_choices, required=False)
    order_status = forms.ChoiceField(order_status_choices, required=False)

    class Meta:
        model = ArikOrder
        fields = ()


class ArikPicksForm(forms.ModelForm):
    date_from = forms.DateField(widget=forms.DateInput())
    date_to = forms.DateField(widget=forms.DateInput())

    class Meta:
        model = ArikOrder
        fields = ()

