from django.contrib import admin
from reports.apps.arik.models import Connections, ArikOrder
from sqlalchemy_utils import database_exists
from reports.apps.arik.tasks import all_orders
from cuser.middleware import CuserMiddleware
from .forms import ConnectionsForm  # reports.apps.arik.forms
import reports.settings as proj_settings
from django.contrib.admin import DateFieldListFilter
from import_export.admin import ImportExportActionModelAdmin
from daterange_filter.filter import DateRangeFilter
from django.contrib.admin import BooleanFieldListFilter


class ConnectionAdmin(admin.ModelAdmin):
    list_display = ("title",)
    form = ConnectionsForm

    def transform_all_orders(self, request, queryset):
        for sc in queryset:
            # try to connect
            """
            src_engine = create_engine(
                sc.database_type + '://' + sc.username + ':' + sc.password + '@' + sc.host + ':' + sc.port + '/' +
                sc.database_name)
            """
            if database_exists(sc.database_type + '://' + sc.username + ':' + sc.password + '@' + sc.host + ':' +
                               sc.port + '/' + sc.database_name):
                connection_params = {'database_type': sc.database_type, 'username': sc.username,
                                     'password': sc.password, 'host': sc.host,
                                     'port': sc.port, 'database_name': sc.database_name}
                self.message_user(request,
                                  "%s successfully connected and launched. You will get email "
                                  "when job will done." % sc.title)

                user = CuserMiddleware.get_user()
                local_db = proj_settings.DATABASES['default']
                all_orders.delay(connection_params, local_db, sc.title, sc.machine_name, user)
            else:
                self.message_user(request, "Cant connect to %s. Please check connection object" % sc.title)

    transform_all_orders.short_description = "Transform all orders for current Connection"

    actions = [transform_all_orders]


class ArikOrderAdmin(ImportExportActionModelAdmin, admin.ModelAdmin):
    list_display = (
        "order_id", "order_status", "mail", "changed", "origin", "destination", "flight_type",
        "pnr", "tickets", "passengers", "field_contact_phone_number_value",
        "payment_method", "remote_id", "message", "amount", "currency_code", "transaction_status")
    list_filter = (('changed', DateRangeFilter), "order_status", "origin", "destination", "flight_type",
                   "passengers", "payment_method", "currency_code", "transaction_status",
                   )

admin.site.register(Connections, ConnectionAdmin)
admin.site.register(ArikOrder, ArikOrderAdmin)

