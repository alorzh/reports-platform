import pandas as pd
import phpserialize
from collections import Iterable
from .models import ArikOrder
from datetime import datetime


def index(src_engine, local_db_engine, machine_name):
    """
    This function will transform data from remote database to
    one table in local database.
    Based on a platform machine name. But need move it to
    Connection model.
    """
    latest_oid = ArikOrder.objects.latest('order_id')
    loid = latest_oid.order_id
    print(loid)
    if loid is None:
        loid = 0

    # connection_id = Connections(machine_name=machine_name).pk
    print('DC trs start')

    """
    df1 = pd.DataFrame(
        columns=['order_id', 'mail', 'order_status', 'changed', 'origin', 'destination', 'flight_type', 'pnr',
                 'tickets', 'passengers', 'field_contact_phone_number_value', 'payment_method',
                 'remote_id', 'message', 'amount', 'currency_code', 'transaction_status',
                 'transaction_remote_status'])
    """

    transactions = pd.read_sql_query(
        'SELECT commerce_order.order_id,'
        'commerce_order.mail, '
        'commerce_order.status AS order_status,'
        'commerce_order.created,'
        'commerce_order.changed,'
        'commerce_order.data, '
        'field_data_field_pnr.field_pnr_value, '
        'field_data_field_contact_phone_number.field_contact_phone_number_value, '
        'commerce_payment_transaction.payment_method, '
        'commerce_payment_transaction.remote_id,'
        'commerce_payment_transaction.message,'
        'commerce_payment_transaction.amount,'
        'commerce_payment_transaction.currency_code,'
        'commerce_payment_transaction.status AS transaction_status,'
        'commerce_payment_transaction.remote_status AS transaction_remote_status '
        'FROM commerce_order '
        'LEFT JOIN field_data_field_pnr ON field_data_field_pnr.entity_id = commerce_order.order_id '
        'LEFT JOIN field_data_commerce_customer_billing '
        'ON field_data_commerce_customer_billing.entity_id = commerce_order.order_id '
        'LEFT JOIN field_data_field_contact_phone_number '
        'ON field_data_field_contact_phone_number.entity_id = field_data_commerce_customer_billing.commerce_customer_billing_profile_id '
        'LEFT JOIN commerce_payment_transaction '
        'ON commerce_payment_transaction.order_id = commerce_order.order_id WHERE commerce_order.order_id > %s  '
        'ORDER BY commerce_order.order_id DESC' % loid,
        src_engine)

    """
    Create new table for orders
    """
    print('New orders start')
    ord_df = transactions[
        ['order_id', 'mail', 'order_status', 'created', 'changed', 'data', 'field_pnr_value',
         'field_contact_phone_number_value',
         'payment_method', 'remote_id', 'message', 'amount', 'currency_code', 'transaction_status',
         'transaction_remote_status']]
    origin = ''
    destination = ''
    for i, row in ord_df.iterrows():
        un_obj = phpserialize.unserialize(row['data'])

        # tickets
        if bool(row['data']) is not False and b'tickets' in un_obj:
            s = []
            if isinstance(un_obj[b'tickets'], Iterable) is True:
                for ticket in un_obj[b'tickets']:
                    if isinstance(un_obj[b'tickets'][ticket], Iterable) is True:
                        for n in un_obj[b'tickets'][ticket]:
                            try:
                                if len(str(un_obj[b'tickets'][ticket][n])) > 2:
                                    print(un_obj[b'tickets'][ticket][n])
                                    s.append(un_obj[b'tickets'][ticket][n])
                            except IndexError:
                                print(un_obj[b'tickets'][ticket])
                                # s.append(un_obj[b'tickets'][ticket])
                    elif isinstance(un_obj[b'tickets'][ticket], str) is True:
                        s.append(un_obj[b'tickets'][ticket])

            row['tickets'] = ','.join(str(e) for e in s)  # convert to str first
            row['passengers'] = len(s)
        else:
            row['passengers'] = 0
            row['tickets'] = ''

        # write flight routes here
        if bool(row['data']) is not False and b'segments' in un_obj:
            if b'origin' in un_obj[b'segments'][0]:
                origin = un_obj[b'segments'][0][b'origin'].decode('UTF-8')  # OW origin
            if b'departureLocation' in un_obj[b'segments'][0]:
                origin = un_obj[b'segments'][0][b'departureLocation'].decode('UTF-8')  # OW origin old style

            if b'destination' in un_obj[b'segments'][0]:
                destination = un_obj[b'segments'][0][b'destination'].decode('UTF-8')  # OW destination

            if b'arrivalLocation' in un_obj[b'segments'][0]:
                destination = un_obj[b'segments'][0][b'arrivalLocation'].decode('UTF-8')  # OW destination old style
            row['origin'] = origin
            row['destination'] = destination

            segm_len = len(un_obj[b'segments'])

            # Check flight type
            if segm_len == 1:
                row['flight_type'] = 'oneway'
            elif segm_len == 2:
                row['flight_type'] = 'roundtrip'
            elif segm_len > 2:
                row['flight_type'] = 'mulidestination'
            else:
                row['flight_type'] = ''

            created = datetime.fromtimestamp(row['created'])
            changed = datetime.fromtimestamp(row['changed'])

            new_df_2 = pd.DataFrame(
                [[int(row['order_id']), row['mail'], row['order_status'], created, changed, row['origin'],
                  row['destination'],
                  row['flight_type'], row['field_pnr_value'], row['tickets'], row['passengers'],
                  row['field_contact_phone_number_value'], row['payment_method'],
                  row['remote_id'], row['message'], row['amount'], row['currency_code'], row['transaction_status'],
                  row['transaction_remote_status']]],
                columns=['order_id', 'mail', 'order_status', 'created', 'changed', 'origin', 'destination',
                         'flight_type', 'pnr',
                         'tickets', 'passengers', 'field_contact_phone_number_value', 'payment_method',
                         'remote_id', 'message', 'amount', 'currency_code', 'transaction_status',
                         'transaction_remote_status'],
                index=[row['order_id']])
        else:
            created = datetime.fromtimestamp(row['created'])
            changed = datetime.fromtimestamp(row['changed'])

            new_df_2 = pd.DataFrame(
                [[int(row['order_id']), row['mail'], row['order_status'], created, changed, '', '',
                  '', row['field_pnr_value'], row['tickets'], row['passengers'],
                  row['field_contact_phone_number_value'], row['payment_method'],
                  row['remote_id'], row['message'], row['amount'], row['currency_code'], row['transaction_status'],
                  row['transaction_remote_status']]],
                columns=['order_id', 'mail', 'order_status', 'created', 'changed', 'origin', 'destination',
                         'flight_type', 'pnr',
                         'tickets', 'passengers', 'field_contact_phone_number_value', 'payment_method',
                         'remote_id', 'message', 'amount', 'currency_code', 'transaction_status',
                         'transaction_remote_status'],
                index=[row['order_id']])
        # print(row['order_id'])
        new_df_2.to_sql(machine_name, local_db_engine, flavor='mysql',
                        if_exists='append')

    print('New orders end')

    return True
