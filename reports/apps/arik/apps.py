from django.apps import AppConfig


class ArikConfig(AppConfig):
    name = 'reports.apps.arik'
