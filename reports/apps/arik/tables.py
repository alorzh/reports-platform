import django_tables2 as tables
from .models import ArikOrder


class TopTable(tables.Table):
    class Meta:
        model = ArikOrder
        # add class="paleblue" to <table> tag
        template = 'django_tables2/bootstrap.html'
        attrs = {'class': 'paleblue'}
