from django.db import models


class Connections(models.Model):
    title = models.CharField(max_length=200)
    database_type = models.CharField(max_length=200, default='mysql')
    database_name = models.CharField(max_length=200, default='', help_text='Name of database which consist raw tables')
    username = models.CharField(max_length=200, default='root')
    password = models.CharField(max_length=200, default='root')
    host = models.CharField(max_length=200, default='localhost')
    port = models.CharField(max_length=200, default='3306')
    machine_name = models.CharField(max_length=300, default='', blank=False)

    class Meta:
        ordering = ("-title",)
        verbose_name = u'Connection'
        verbose_name_plural = u'Connections'

    def __str__(self):
        return self.title


class ArikOrder(models.Model):
    """
    For every connection we should create new model.
    Maybe we should do it dynamically?
    """
    index = models.IntegerField(primary_key=True, db_index=True, unique=True)
    order_id = models.IntegerField(unique=True)
    mail = models.EmailField()
    order_status = models.CharField(max_length=50, default='', blank=True)
    created = models.DateTimeField()
    changed = models.DateTimeField()
    origin = models.CharField(max_length=50, default='', blank=True)
    destination = models.CharField(max_length=50, default='', blank=True)
    flight_type = models.CharField(max_length=50, default='', blank=True)
    pnr = models.CharField(max_length=50, default='')
    tickets = models.CharField(max_length=250, default='')
    passengers = models.IntegerField(blank=True, default=0)
    field_contact_phone_number_value = models.CharField(max_length=50, default='')
    payment_method = models.CharField(max_length=50, default='')
    remote_id = models.CharField(max_length=50, default='')
    message = models.CharField(max_length=150, default='')
    amount = models.IntegerField(blank=True, default=0)
    currency_code = models.CharField(max_length=50, default='')
    transaction_status = models.CharField(max_length=50, default='')
    transaction_remote_status = models.CharField(max_length=150, default='')
    # connection = models.ForeignKey(Connections, default='1')

    class Meta:
        ordering = [
            "-order_id"]
        verbose_name = u'Arik Order'
        verbose_name_plural = u'Arik Orders'

        managed = False
        db_table = 'arikair'  # or will try load it from connection

    def __str__(self):
        title = 'Order ID: ' + str(self.order_id)
        return title




