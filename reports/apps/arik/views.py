from django.shortcuts import render
from .models import ArikOrder
from .forms import ArikTopNRoutesForm, ArikPicksForm
from chartit import DataPool, Chart, PivotDataPool, PivotChart
from django.views.decorators.cache import cache_page
import pandas as pd
from django.db.models import Q
from .tables import TopTable
from django_tables2 import RequestConfig


def index(request):
    context_dict = {}
    form = ArikTopNRoutesForm()
    context_dict['form'] = form
    source = ArikOrder.objects.filter(order_status='completed')[:10]
    if request.method == 'POST':
        form = ArikTopNRoutesForm(request.POST)
        if form.is_valid():
            form_values = {'changed__gte': form.cleaned_data['date_from'], 'changed__lte': form.cleaned_data['date_to'],
                           'order_status': form.cleaned_data['order_status'], 'origin': form.cleaned_data['origin'],
                           'destination': form.cleaned_data['destination'],
                           'flight_type': form.cleaned_data['flight_type']}
            arguments = {}
            for k, v in form_values.items():
                if v:
                    arguments[k] = v
            source = ArikOrder.objects.filter(**arguments)[:form.cleaned_data['top_n']]

            orders = \
                DataPool(
                    series=
                    [{'options': {
                        'source': source},
                        'terms': [
                            "origin", "destination"]}])
            # Step 2: Create the Chart object
            cht = Chart(
                datasource=orders,
                series_options=
                [{'options': {
                    'type': 'column',
                    'stacking': False},
                    'terms': {
                        'origin': [
                            "origin"]
                    }}],
                chart_options=
                {'title': {
                    'text': 'Arik Orders data'},
                    'xAxis': {
                        'title': {
                            'text': 'Origin'}}})
            context_dict['cht'] = cht

            return render(request, 'arik.html', context_dict)

    orders = \
        DataPool(
            series=
            [{'options': {
                'source': source},
                'terms': [
                    "order_status", "origin", "destination", "flight_type",
                    "passengers", "payment_method", "currency_code", "transaction_status"]}])
    # Step 2: Create the Chart object
    cht = Chart(
        datasource=orders,
        series_options=
        [{'options': {
            'type': 'column',
            'stacking': False},
            'terms': {
                'origin': [
                    "order_status", "origin", "destination", "flight_type",
                    "passengers", "payment_method", "currency_code", "transaction_status"]
            }}],
        chart_options=
        {'title': {
            'text': 'Arik Orders data'},
            'xAxis': {
                'title': {
                    'text': 'Changed time'}}})

    # Step 3: Send the chart object to the template.
    context_dict['cht'] = cht

    return render(request, 'arik.html', context_dict)


"""
def top_n_orders(request):

    orders = ArikOrder.objects.filter(changed__gte='2016-06-23', changed__lte='2016-06-24', transaction_status='failure'
                                      ).exclude(order_status='completed').distinct()
    unique = orders.distinct()
    c = unique.count()

    form = ArikTopNRoutesForm()
    context_dict = {}
    orders = ArikOrder.objects.filter(order_status='completed')[:100]
    table = TopTable(orders)
    RequestConfig(request, paginate={'per_page': 25}).configure(table)
    context_dict['form'] = form
    #context_dict['table'] = table

    return render(request, 'arik.html', context_dict)
"""
